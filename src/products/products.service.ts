import { Injectable } from '@nestjs/common';
import { CreateProductDto } from './dto/product.dto';

import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';

import { Product } from './interfaces/product.interface';

@Injectable()
export class ProductsService {
  constructor(
    @InjectModel('Product') private readonly productModel: Model<Product>,
  ) {}

  async create(createProductDto: CreateProductDto): Promise<Product> {
    const product = new this.productModel(createProductDto);
    return await product.save();
  }

  async findAll(): Promise<Product[]> {
    const products = await this.productModel.find();
    return products;
  }

  async findOne(productId: string): Promise<Product> {
    const product = await this.productModel.findById(productId);
    return product;
  }

  async update(
    productId: string,
    createProductDto: CreateProductDto,
  ): Promise<Product> {
    const update = await this.productModel.findByIdAndUpdate(
      productId,
      createProductDto,
      { new: true },
    );
    return update;
  }

  async remove(productId: string): Promise<any> {
    const remove = await this.productModel.findByIdAndDelete(productId);
    return remove;
  }
}
