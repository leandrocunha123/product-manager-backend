import {
  Controller,
  Get,
  Post,
  Body,
  Put,
  Param,
  Delete,
  HttpStatus,
  Res,
  NotFoundException,
  Query,
} from '@nestjs/common';
import { ProductsService } from './products.service';
import { CreateProductDto } from './dto/product.dto';

@Controller('products')
export class ProductsController {
  constructor(private readonly productsService: ProductsService) {}

  @Post('/create')
  async create(@Res() res, @Body() createProductDto: CreateProductDto) {
    const product = await this.productsService.create(createProductDto);
    return res.status(HttpStatus.OK).json({
      message: 'Product successfully created',
      product,
    });
  }

  @Get('/list')
  async findAll(@Res() res) {
    const products = await this.productsService.findAll();
    return res.status(HttpStatus.OK).json(products);
  }

  @Get('/:productId')
  async findOne(@Res() res, @Param('productId') productId) {
    const product = await this.productsService.findOne(productId);
    if (!product) throw new NotFoundException('Product does not exist!');
    return res.status(HttpStatus.OK).json(product);
  }

  @Put('/update')
  async update(
    @Res() res,
    @Body() createProductDto: CreateProductDto,
    @Query('productId') productId,
  ) {
    const updated = await this.productsService.update(
      productId,
      createProductDto,
    );
    if (!updated) throw this.productsService.update(productId, createProductDto);
    return res.status(HttpStatus.OK).json({
      message: 'Product updated with sucessfully',
      updated,
    });
  }

  @Delete('/delete')
  async remove(@Res() res, @Query('productId') productId) {
    const removed = await this.productsService.remove(productId);
    if (!removed) throw new NotFoundException('Product does not exist');
    return res.status(HttpStatus.OK).json({
      message: 'Product deleted sucessfully',
      removed,
    });
  }
}
